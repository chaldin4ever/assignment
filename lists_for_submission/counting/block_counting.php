<?php

// block count user

class block_counting extends block_base {

    public function init() {
        $this->title = get_string('pluginname', 'block_counting');
    }
    
    
  public function applicable_formats() {
	  return array('site' => true);
	}

    public function specialization() {
	  if (!empty($this->config->title)) {
	    $this->title = $this->config->title;
	  } else {
	    $this->config->title = 'Persistent Counting';
	  }
	 
    }

    public function get_content() {
    
            global $DB, $PAGE, $CFG;
            
            $PAGE->requires->js('/blocks/counting/javascript/jquery-1.11.1.min.js');
    
	    if ($this->content !== null) {
	      return $this->content;
	    }
	    
	       if($_POST){
 	       
 	       $add = $_POST['add'];
 	       $currentTotal = $_POST['current'];
 	       
 	       $value = $currentTotal + $add;  	            
 	       
 	       if($currentTotal > 0){	      
 	       
 	       		$update = "Update {count} set count='$value'";
 	       		$DB->execute($update);
 	       		
 	       	}else{
 	       
 	       		$insert =  "INSERT INTO {count}(count) VALUES('$value')";	       		
 	       		$DB->execute($insert);
 	       	}
 	        	      	       	         
 	    }
	 
	    $select = $DB->get_records_sql('SELECT count FROM {count}');
	    
	    $this->content = new stdClass;
	    
	    foreach ($select as $count){
	    	
	    	$totalCount = $count->count;
	    }
	    
	    if($totalCount > 0){
	    
	    	$total = $totalCount;
	    	
	    }else{
	    
	    	$totalCount=0;
	    
	    	$total = $totalCount;
	  
	    } 

	    $this->content->text =  '<div id="countTotal"> </div>';    
	    	               
	    // $url = $_SERVER['PHP_SELF'];
	    
	    $style =' style = "cursor: default; border: transparent; background: transparent; padding: 5px;  width: 25px;" ';
	    
	    $url = $this->page->url;
	    
	    $this->content->text .=   '<form name="form" class="count" id="count" method="post" action="'.$url.'" >';
	    
	    $this->content->text .= 'Total Count: <input '.$style.' type="input" readonly="readonly" name="current" value="'.$total.'" /> <br /> ';
	    	     
	    $this->content->text .= '<input type="button" value="AJAX Submit" name="submit" onclick="viewCount();" /> ';
	    
	    $this->content->text .= '<input type="submit" value="POST Submit" name="submit"  /> ';
	     
	    $this->content->text .= '<input type="hidden" name="add" value="1" /> ';

	    
            $this->content->text .= "</form>";
                               
            $this->content->text .= '          
            
            <script type="text/javascript">

                                 function viewCount(){
                                                                  
                                     $.post(
                                     
                                     "'.$CFG->wwwroot.'/blocks/counting/count.php",
                                     {add: form.add.value},
                                     function(output){$("#countTotal").html(output).show();}
                                     
                                     );
                                     
                                      var addValue = parseInt($("input[name=current]").val());
                                      addValue++;
				     				  $( "input[name=current]" ).val( addValue ); 

                                }
                                         
            </script>

            ';
            
            
 	    
 	   	
	    return $this->content;
		
	  }	  
	  
}  