<?php

// block user lists and course

class block_lists extends block_base {

    public function init() {
        $this->title = get_string('pluginname', 'block_lists');
    }
    
    
  public function applicable_formats() {
	  return array('site' => true);
	}

    public function specialization() {
	  if (!empty($this->config->title)) {
	    $this->title = $this->config->title;
	  } else {
	    $this->config->title = 'Kaplan Plugin';
	  }
	 
    }

    public function get_content() {
    
            global $DB;                 
    
	    if ($this->content !== null) {
	      return $this->content;
	    }
	    
	    // get the registered users from the user table
		
	    $users = $DB->get_records_sql('SELECT firstname, lastname FROM {user}');
	   
	    // get all the students and courses,. then count the username group by course id
	    $courses = $DB->get_records_sql('SELECT
						c.id AS courseid, 
						c.fullname, 
						count(u.username) as student
						                                
						FROM 
						{role_assignments} ra 
						JOIN {user} u ON u.id = ra.userid
						JOIN {role} r ON r.id = ra.roleid
						JOIN {context} cxt ON cxt.id = ra.contextid
						JOIN {course} c ON c.id = cxt.instanceid
						
						WHERE ra.userid = u.id
						                                
						AND ra.contextid = cxt.id
						AND cxt.contextlevel =50
						AND cxt.instanceid = c.id
						AND  roleid = 5
						
						GROUP BY c.id');
	    
	    $this->content = new stdClass;
	    
	    $this->content->text =  '<h4>Users</h4> ';
	    
	    $this->content->text .=  '<ul> '; 
	    
		//list all users
	    foreach ($users as $list){
	    	
	    	 $this->content->text .= '<li>'.$list->firstname.' '.$list->lastname.'</li>';
	    }
	    	    
            $this->content->text .= "</ul>";
            
            $this->content->text .=  '<h4>Courses</h4> ';
            
             $this->content->text .=  '<ul> '; 
	    
		//lists all courses with total number of students per course
	    foreach ($courses as $course){
	    	
	    	 $this->content->text .= '<li>'.$course->fullname.'('.$course->student.')</li>';
	    }
	    	    
            $this->content->text .= "</ul>";
                                            	
	    return $this->content;
		
	  }	  
	  
}  